require File.expand_path("test/test_helper")
require File.expand_path("lib/beyond/scraper")

module Beyond
  class ScraperTest < Minitest::Test
    def setup
      @beyond = Scraper.new
      @keywords = "web developer"
    end

    def test_job_function_filtering
      job_functions = ["Information Technology", "Research"]
      jobs = @beyond.job_search(@keywords, limit: 1, job_functions: job_functions)
      assert_equal 1, jobs.size
    end

    def test_getting_three_jobs
      jobs = @beyond.job_search(@keywords, limit: 3)
      assert_equal 3, jobs.size
    end

    def test_variations
      variations = %(contract freelance)
      jobs = @beyond.job_search(@keywords, limit: 1, variations: variations)
      assert_equal 1, jobs.size
    end

    def test_max_page_option
      jobs = @beyond.job_search(@keywords, max_page: 1, limit: 100)
      assert_operator jobs.size, :<=, 50
    end

    def test_passing_in_a_callback_to_job_search
      job = nil
      @beyond.job_search(@keywords, limit: 1) do |j|
        job = j
      end
      refute_nil job
    end
  end
end

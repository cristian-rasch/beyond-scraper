require_relative "scraper/version"
require "capybara/poltergeist"
Capybara.match = :first
require "open-uri"
require "dotenv"
Dotenv.load
require "logger"
require "chronic"

module Beyond
  class Scraper
    BASE_URL = "http://www.beyond.com/".freeze
    SEARCH_URL = URI.join(BASE_URL, "/jobs/search/advanced").to_s.freeze
    JOB_REDIRECT_PATH = %r(/jobs/job/view\?url=http)
    PAGE_PARAM_REGEXP = /&p=\d+/
    DEFAULT_LIMIT = 100
    DEFAULT_VARIATIONS = ["remote", "telecommute", "telecommuting", "work from home", "contract", "freelance"].freeze
    MAX_PAGE = 25
    PHANTOMJS_TIMEOUT = 60 # seconds
    USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0".freeze
    DATE_FORMAT =  "%m/%d/%Y".freeze
    DATE_REGEXP = %r((\d{2}/\d{2}/\d{4}))

    class Job < Struct.new(:title, :location, :employer, :short_description, :created_at, :url,
                           :id, :industry, :description_html, :updated_on)
    end

    class Proxy < Struct.new(:host, :port); end

    # Options include:
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - variations - a list of suffixes to append to the searched keywords, e.g. %w(remote freelance contract)
    #   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
    #   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
    #               defaults to reading them from the BEYOND_PROXIES env var or nil if missing
    #   - logger - a ::Logger instance to log error messages to
    def initialize(options = {})
      @options = options
      @env = ENV["RACK_ENV"] || ENV["RAILS_ENV"] || "development"
      configure_logger(options[:logger])
      configure_proxies(options[:proxies])
    end

    def job_functions
      @job_functions ||= begin
                           if visit_through_proxy(SEARCH_URL)
                             search_page = Nokogiri::HTML(@session.html)
                             functions = search_page.css("#FCareerFocus1 option").map(&:text)
                             functions.shift
                             functions
                           else
                             []
                           end
                         end
    end

    # Options include:
    #   - job_functions - one or two job function filters to apply to the search.
    #                     @see the occupations returned by the #job_functions method.
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - variations - a list of suffixes to append to the searched keywords, e.g. %w(remote freelance contract)
    #   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
    #   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
    #               defaults to reading them from the BEYOND_PROXIES env var or nil if missing
    #   - callback - an optional block to be called with each job found ASAP
    def job_search(keywords, options = {})
      limit = (options[:limit] || @options[:limit] || DEFAULT_LIMIT)
      limit = [limit, DEFAULT_LIMIT].min
      max_page = options[:max_page] || @options[:max_page] || MAX_PAGE
      max_page = [max_page, MAX_PAGE].min
      variations = if options.has_key?(:variations)
                     options[:variations]
                   elsif @options.has_key?(:variations)
                     @options[:variations]
                   else
                     DEFAULT_VARIATIONS
                   end
      variations = Array(variations)
      variations << "" if variations.empty?
      occupations = Array(options[:job_functions] || @options[:job_functions])
      occupations &= job_functions
      kws = Array(keywords).join(" ")
      jobs = []
      current_page = 1

      catch(:done) do
        begin
          empty = variations.map do |variation|
            next(true) unless visit_through_proxy(SEARCH_URL)

            @session.within('form[name="F"]') do
              @session.fill_in "FKeywords", with: "#{variation} #{kws}"
              @session.select occupations[0], from: "FCareerFocus1" if occupations.size > 0
              @session.select occupations[1], from: "FCareerFocus2" if occupations.size > 1
              @session.click_on "Search Jobs"
            end

            jobs_page = if current_page > 1
                          Nokogiri::HTML(@session.html)
                          path = jobs_page.at_css(".pagination .active").next_element.at_css("a")["href"]
                          path.sub!(PAGE_PARAM_REGEXP, "&p=#{current_page}")
                          url = URI.join(BASE_URL, path)
                          io = open_through_proxy(url)
                          next unless io

                          Nokogiri::HTML(io)
                        else
                          Nokogiri::HTML(@session.html)
                        end
            job_nodes = jobs_page.css(".job")

            if job_nodes.empty?
              true
            else
              job_nodes.each do |job_node|
                title_link = job_node.at_css('[itemprop="url"]')
                job_url = title_link["href"]
                next if job_url =~ JOB_REDIRECT_PATH

                title = title_link.text
                id = job_url[/-(\d+)-job[.]html\z/, 1]
                location_node = job_node.at_css('[itemprop="addressLocality"]')
                location = location_node.text if location_node
                employer = job_node.at_css('[itemprop="name"]').text
                short_description = job_node.at_css('[itemprop="description"]').text.strip
                created_at = Chronic.parse(job_node.at_css('[itemprop="datePosted"]').text, context: :past).utc

                if io = open_through_proxy(job_url)
                  job_page = Nokogiri::HTML(io)
                  industry_node = job_page.at_css('[itemprop="industry"]')
                  industry = industry_node["industry"] if industry_node
                  description_node = job_page.at_css('[itemprop="description"]')
                  description_html = ""
                  begin
                    until Array((description_node = description_node.next_element || description_node.parent)["class"]).include?("text-muted")
                      description_html << description_node.to_html.chomp
                    end
                  rescue => err
                    @logger.warn "URL: #{job_url} - #{err.message}"
                  else
                    description_html.gsub!("\n", "")
                    updated_on = parse_date(description_node.text[DATE_REGEXP, 1])
                  end
                end

                job = Job.new(title, location, employer, short_description, created_at, job_url, id, industry,
                              description_html, updated_on)
                yield(job) if block_given?
                jobs << job

                throw(:done) if jobs.size == limit
              end

              false
            end
          end

          done = empty.all?
          current_page += 1
          break if current_page > max_page
        end until done
      end

      jobs
    end

    private

    def configure_logger(logger = nil)
      @logger = logger || @options[:logger] || Logger.new(STDERR)
    end

    def parse_date(date)
      Date.strptime(date, DATE_FORMAT)
    end

    def configure_proxies(proxies = nil)
      @proxies = (proxies || @options[:proxies] || ENV.fetch("BEYOND_PROXIES").split(",")).shuffle.map do |proxy|
        uri = URI(proxy)
        Proxy.new(uri.host, uri.port).tap do |proxy|
          Capybara.register_driver :"poltergeist_#{uri.host}" do |app|
            Capybara::Poltergeist::Driver.new(app, timeout: PHANTOMJS_TIMEOUT,
                                              phantomjs_logger: @env == "development" ? STDOUT : Logger.new("/dev/null"),
                                              js_errors: false,
                                              phantomjs_options: ["--proxy=#{proxy.host}:#{proxy.port}", "--load-images=no"])
          end
        end
      end
    end

    def visit_through_proxy(url)
      new_session
      begin
        @session.visit(url)
      rescue => err
        @logger.error "#{err.message} visiting '#{url}'"
        nil
      end
    end

    def open_through_proxy(url)
      proxy = next_proxy
      begin
        open(url, "User-Agent" => USER_AGENT, "proxy" => "http://#{proxy.host}:#{proxy.port}/")
      rescue => err
        @logger.error "#{err.message} opening '#{url}'"
        nil
      end
    end

    def next_proxy
      @proxies.shift.tap { |proxy| @proxies << proxy }
    end

    def new_session
      @session = Capybara::Session.new(:"poltergeist_#{next_proxy.host}").tap do |session|
        session.driver.headers = { "User-Agent" => USER_AGENT }
      end
    end
  end
end

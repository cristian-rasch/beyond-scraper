# Beyond::Scraper

beyond.com job scraper

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'beyond-scraper', git: 'git@bitbucket.org:cristian-rasch/beyond-scraper.git', require: 'beyond/scraper'
```

And then execute:

    $ bundle

### Installing the PhantomJS headless browser

**IMPORTANT** this library relies on having the PhantomJS binary in your PATH. Make sure to follow the [installation instructions](https://github.com/teampoltergeist/poltergeist#installing-phantomjs).


## Usage

```ruby
require "beyond/scraper"
require "pp"

beyond = Beyond::Scraper.new
# Options include:
#   - limit - how many jobs to retrieve (defaults to 100, max. 100)
#   - variations - a list of suffixes to append to the searched keywords, e.g. %w(remote freelance contract)
#   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
#   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
#               defaults to reading them from the BEYOND_PROXIES env var or nil if missing
#   - callback - an optional block to be called with each job found ASAP
pp beyond.job_search("web developer", variations: %w(contract freelance), limit: 1)
#  #<struct Beyond::Scraper::Job
#   title="Web Designer",
#   location="San Diego",
#   employer="TalentBurst",
#   short_description=
#    "Position # 4138612 Title: Web Designer Location: San Diego, CA Duration: 10+ Months contract As a Web Designer at Illumina, you will have strong design skills and front",
#   created_at=2015-02-14 20:26:55 UTC,
#   url=
#    "http://www.beyond.com/jobs/web-designer-san-diego-ca-235259267-job.html",
#   id="235259267",
#   industry=nil,
#   description_html=
#    "<p>As a Web Designer at Illumina, you will have strong design skills and front-end web development abilities. You are detail oriented, process-friendly, and have the ability to work within tight time-lines on multiple projects simultaneously. </p><p>We're looking for: </p><p>Modern, clean design. Think www.apple.com , or www.adobe.com </p><p>Rich interactive pieces example: ~~~/ (scroll down the page) </p><p>Design and coding that works in all browsers, including Internet Explorer 8, Chrome, Safari, Firefox </p><p>Responsibilities will include but will not be limited to: </p><p>Create graphics, banners, landing pages, and web sites </p><p>Design layout and templates for maximum usability and focus on user experience </p><p>Create interactive pieces using HTML, CSS, and jQuery </p><p>Use a web content management system in order to edit/create pages, assist partner departments with adding/editing content in the CMS </p><p>Create HTML emailers using Marketo or other enterprise level marketing automation platforms </p><p>Implement cookie code for analytics and dynamic content purposes </p><p>Give expert design advice on projects, make recommendations for improving web sites </p><p>Work with web developers to apply style and user interface design to web applications </p><p>Apply Illumina s brand guidelines </p><p>Work under the direction of other internal Marketing teams in addition to the Digital Experience Team </p><p>Skills: </p><p>Required Skills </p><p>jQuery </p><p>Photoshop </p><p>Hand-coding HTML and CSS3 </p><p>Desired Skills </p><p>Responsive design (mobile-friendly)</p><p>Company Description:</p><p>TalentBurst is an award winning IT, Accounting &amp; Finance staffing firm headquartered in Boston with offices in San Francisco, Miami, Milwaukee, Toronto, Bangalore, and Gurgaon. Our clients include the leading social media, technology, banks, utilities, pharmaceutical, and biotech companies.</p><p>We work with 75 of the Fortune 500 companies. Our recruiting team is one of the most successful in the industry and has a strong reputation for treating our employees well and helping you find your next dream project.</p><p>Benefits</p><p>At TalentBurst, one of our founding principles has been to provide our employees with the best benefits available. Benefits we have available include:</p><p>• Health, Dental, and Vision Plan</p><p>• 401(k) Plan</p><p>• Life Insurance</p><p>• Immigration assistance</p><p>• Credit Union Membership</p><p>• Direct Deposit</p><p>• Vacation pay (only for FTEs)</p>"
```

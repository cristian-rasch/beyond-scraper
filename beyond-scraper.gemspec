# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'beyond/scraper/version'

Gem::Specification.new do |spec|
  spec.name          = "beyond-scraper"
  spec.version       = Beyond::Scraper::VERSION
  spec.authors       = ["Cristian Rasch"]
  spec.email         = ["cristianrasch@fastmail.fm"]
  spec.summary       = %q{beyond.com job scraper}
  spec.homepage      = "https://bitbucket.org/cristian-rasch/beyond-scraper"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "dotenv", "~> 1.0"
  spec.add_runtime_dependency "poltergeist", "~> 1.6"
  spec.add_runtime_dependency "chronic", "~> 0.10"

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.5"
  spec.add_development_dependency "minitest-line", "~> 0.6"
end
